"use client";
import React, { useEffect, useState } from "react";
import { Tabs, TabsProps } from "antd";
import { useRouter } from "next/navigation";
import CardItem from "../CardItem/CardItem";
import Link from "next/link";
import Image from "next/image";
import ListProduct from "../ListProduct/ListProduct";
import productSanDa from "../../../utils/product.json";
const TuyenDungComponent = (props: any) => {
  console.log("productSanDa?.tuVanThongTin", productSanDa?.tuVanThongTin);
  return (
    <div className="w-full h-auto">
      <div>
        <div className="bg-[url('/image/tuvanthicong/banner-tuvantt.jpg')] bg-cover w-full h-[700px] font-bold flex text-[#05372e] justify-center items-center text-4xl text-center">
          THÔNG TIN TUYỂN DỤNG
        </div>
        <div>
          <div className="flex justify-center">
            <div className="flex w-10/12 flex-col my-3 justify-center items-center">
              <div className="h-96 w-96 shadow-2xl bg-white rounded-2xl">
                <h3 className="text-center text-2xl rounded-t-2xl bg-[#f1592a] text-white h-14 p-0 flex justify-center items-center">
                  Thông báo
                </h3>
                <div className="flex h-72 justify-center items-center text-md font-medium p-4 flex-col text-[#05372e] text-center">
                  <p>Công ty hiện đã đủ vị trí nên chưa thông tin tuyển dụng</p>
                  <p>
                    Hãy theo dõi thông tin để có thể thấy thông tin tuyển dụng
                    sớm nhất
                  </p>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default TuyenDungComponent;
