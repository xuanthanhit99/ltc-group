"use client";

import { Card, Carousel, Input, notification } from "antd";
import { GetServerSideProps } from "next";
import ListProduct from "../ListProduct/ListProduct";
import Image from "next/image";
import { AuthContextDefault } from "../../../app/context/AuthContext";
import { useEffect } from "react";
import productSanDa from "../../../utils/product.json";
const HomeComponent = ({ productnews, isLoadingProduct }: any) => {
  const { isLoadingAuth } = AuthContextDefault();
  const key = "home";
  const [api, contextHolder] = notification.useNotification();

  const onOpenLoading = () => {
    if (isLoadingAuth) {
      api.open({
        key,
        message: "Đặt hàng thành công",
        description: "Cảm ơn bạn đã đặt hàng tại cửa hàng chúng tôi.",
      });

      setTimeout(() => {
        api.open({
          key,
          message: "Đặt hàng thành công",
          description: "Cảm ơn bạn đã đặt hàng tại cửa hàng chúng tôi.",
        });
      }, 1000);
    }
  };
  useEffect(() => {
    onOpenLoading();
  }, [isLoadingAuth]);

  return (
    <div>
      {contextHolder}
      <div>
        <Carousel autoplay>
          <div>
            <h3 className="lg:h-[330px] sm:h-[150px] md:h-[150px] xl:h-[630px]">
              <img
                src="/image/slider/slider-1.jpg"
                alt=""
                className="w-full h-auto"
              />
            </h3>
          </div>
          <div>
            <h3 className="lg:h-[330px] sm:h-[150px] md:h-[150px] xl:h-[630px]">
              <img
                src="/image/slider/slider-2.jpg"
                alt=""
                className="w-full h-auto"
              />
            </h3>
          </div>
        </Carousel>
      </div>
      <div className="flex justify-center">
        <div className="flex flex-row w-10/12 p-2">
          <div className="basis-1/2 text-[#05372e]">
            <h2 className="text-start font-bold text-4xl my-6">Về chúng tôi</h2>
            <p className="text-start text-xl my-4 leading-8">
              Công ty cổ phần xuất nhập khẩu và thương mại LTC là một trong
              những đơn hàng đầu trong lĩnh vực sản xuất và cung cấp sàn đá công
              nghệ SPC tại Việt Nam. Với hơn 10 năm kinh nghiệm trong ngành sản
              xuất và phân phối sàn đá công nghệ SPC, LTC tự hào là đơn vị có
              đội ngũ nhân viên giàu kinh nghiệm và chuyên môn cao.{" "}
            </p>
            <p className="text-start text-xl my-4 leading-8">
              LTC chuyên sản xuất và cung cấp các loại sàn đá công nghệ cao cấp
              để sử dụng trong nội thất và kiến trúc xây dựng. Sản phẩm của công
              ty chúng tôi được làm từ chất liệu đá tự nhiên, nhựa nguyên sinh
              và hợp chất sợi thủy tinh tổng hợp với công nghệ hiện đại và tiên
              tiến nhất.{" "}
            </p>
            <p className="text-start text-xl my-4 leading-8">
              Sản phẩm sàn đá công nghệ SPC của LTC cung cấp có nhiều ưu điểm
              nổi bật như độ bền cao, chịu nước, chống cháy, chống trầy xước và
              vệ sinh dễ dàng. Ngoài ra, công ty chúng tôi còn cung cấp đa dạng
              mẫu mã và màu sắc để khách hàng có nhiều lựa chọn phù hợp với
              không gian kiến trúc và sở thích cá nhân.{" "}
            </p>
            <p className="text-start text-xl my-4 leading-8">
              LTC cam kết mang đến cho khách hàng sự hài lòng tối đa bằng việc
              chất lượng sản phẩm luôn đảm bảo và dịch vụ sau bán hàng chuyên
              nghiệp. Công ty cũng không ngừng nâng cao công nghệ sản xuất và
              tìm kiếm những sản phẩm phù hợp đáp ứng đa dạng nhu cầu của quý
              khách hàng.{" "}
            </p>
            <p className="text-start text-xl my-4 leading-8">
              Với những giá trị cốt lõi như chất lượng, sáng tạo và tôn trọng
              khách hàng, Công ty cổ phần xuất nhập khẩu và thương mại LTC đang
              ngày càng khẳng định vị thế của mình trên thị trường và trở thành
              đối tác tin cậy của nhiều công trình xây dựng lớn trên toàn quốc…
            </p>
          </div>
          <div className="basis-1/2 flex justify-center items-center flex-col">
            <Image
              src={"/image/slider/slider-3.jpg"}
              width={420}
              height={420}
              alt=""
              className="my-3"
            />
            <Image
              src={"/image/slider/slider-4.jpg"}
              width={420}
              height={420}
              alt=""
              className="my-3"
            />
          </div>
        </div>
      </div>
      <hr className="my-4" />
      {productSanDa && (
        <div className="flex flex-col items-center justify-center">
          <div className="flex w-10/12 flex-col">
            <div className="w-full h-14  text-[#05372e] font-bold flex justify-start border-b border-[#00a2e9]  items-center lg:text-xl sm:text-sm text-center md:text-lg xl:text-4xl">
              <span className="">TOP SẢN PHẨM XU HƯỚNG</span>
            </div>
            <div className="my-3">
              {isLoadingProduct ? (
                <div className="flex justify-center items-center">
                  <Image
                    src="/image/loading-2.gif"
                    width={350}
                    height={350}
                    alt="loader"
                    className="object-contain"
                  />
                </div>
              ) : (
                <ListProduct valueproduct={productSanDa?.productSanDa?.filter((item: any) => item?.key <= 4)} />
              )}
            </div>
          </div>
        </div>
      )}
      <hr className="my-4" />
      {productSanDa && (
        <div className="flex flex-col items-center justify-center">
          <div className="flex w-10/12 flex-col">
            <div className="w-full h-14  text-[#05372e] font-bold flex justify-start border-b border-[#00a2e9]  items-center lg:text-xl sm:text-sm text-center md:text-lg xl:text-4xl">
              <span className="">Tin tức</span>
            </div>
            <div className="my-3">
              {isLoadingProduct ? (
                <div className="flex justify-center items-center">
                  <Image
                    src="/image/loading-2.gif"
                    width={350}
                    height={350}
                    alt="loader"
                    className="object-contain"
                  />
                </div>
              ) : (
                <ListProduct valueproduct={productSanDa.productNews} />
              )}
            </div>
          </div>
        </div>
      )}
      {productSanDa && (
        <div className="flex flex-col items-center justify-center">
          <div className="flex w-10/12 flex-col">
            <div className="w-full h-14  text-[#05372e] font-bold flex justify-center my-6 border-b border-[#00a2e9]  items-center lg:text-xl sm:text-sm text-center md:text-lg xl:text-4xl">
              <span className="py-6">ĐỐI TÁC CỦA CHÚNG TÔI</span>
            </div>
            <div className="my-3 flex flex-wrap justify-center">
              {productSanDa &&
                productSanDa?.productDoiTac?.map((item: any, index: any) => {
                  return (
                      <Image
                        key={index}
                        src={item.image}
                        width={250}
                        height={250}
                        alt="loader"
                        className="object-contain m-4"
                      />
                  );
                })}
            </div>
          </div>
        </div>
      )}
    </div>
  );
};

export const getServerSideProps: GetServerSideProps = async (ctx) => {
  return {
    props: {},
  };
};

export default HomeComponent;
