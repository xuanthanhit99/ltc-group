import { Card } from "antd";
import Link from "next/link";
import React, { useEffect, useState } from "react";
import { AuthContextDefault } from "../../../app/context/AuthContext";
import { VND } from "../../../utils/format";

const ListProduct = ({ valueproduct }: any) => {
  const { onClickrecentlyViewed } = AuthContextDefault();
  console.log("valueproduct", valueproduct);
  return (
    <div className="w-full grid grid-cols-1 gap-1 lg:grid-cols-4 lg:gap-4 sm:grid-cols-1 sm:gap-1 md:grid-cols-2 md:gap-2 xl:grid-cols-4 xl:gap-4 ">
      {valueproduct?.map((item: any) => {
        return (
          <div key={item.key} onClick={() => onClickrecentlyViewed(item)} className="sm:mb-6">
            <Link href={item?.link}>
              <Card
                hoverable
                cover={<img alt="example" src={item?.image} className={`!h-80 `}/>}
                className={`bg-[#05372e] text-white `}
              >
                <div className={`mt-1 flex flex-col justify-between items-center h-20 w-full ${item?.type === "Tin tức" && "h-[165px]"} ${item?.type === "Tư Vấn Thông tin" || item?.type === "Dự án đã thực hiện" && "h-[156px]"}`} >
                  {item?.type !== "Tin tức" && <p className="font-medium	text-sx text-center text-[#99aba8]">
                    ROYAL CRYSTAL
                  </p>}
                  <h5 className={"text-base font-medium	text-center"}>
                    {item?.label}
                  </h5>
                  {item?.type === "Tin tức" || item?.type === "Tư Vấn Thông tin" || item?.type === "Dự án đã thực hiện" && <hr />}
                  <p className="my-3">{item?.type === "Tin tức" && item?.description}</p>
                  {item?.type === "Tin tức" || item?.type === "Tư Vấn Thông tin" || item?.type === "Dự án đã thực hiện"&& <div className="w-full font-medium	text-sx text-center text-[#05372e] flex justify-center items-center bg-white h-12 rounded-xl">
                    Đọc thêm
                  </div>}
                </div>
              </Card>
            </Link>
          </div>
        );
      })}
    </div>
  );
};

export default ListProduct;
