"use client";
import React, { useEffect, useState } from "react";
import { Tabs, TabsProps } from "antd";
import { useRouter } from "next/navigation";
import CardItem from "../CardItem/CardItem";
import Link from "next/link";
import Image from "next/image";

const ServiceComponent = (props: any) => {
  return (
    <div className="w-full h-auto">
      <div>
        <div className="bg-[url('/image/banner-gioithieu.jpg')] bg-cover w-full h-[700px] font-bold flex text-[#05372e] justify-center items-center text-4xl text-center">
          SÀN ĐÁ CÔNG NGHỆ SPC
        </div>
        <div className="flex flex-col items-center justify-center">
          <div className="flex w-10/12 flex-col">
            <div className="w-full h-14  text-[#05372e] font-bold flex justify-center my-6 border-b border-[#00a2e9]  items-center lg:text-xl sm:text-sm text-center md:text-lg xl:text-4xl">
              <span className="py-6">CÔNG NGHỆ SPC ROYAL</span>
            </div>
            <div className="w-full grid grid-cols-2 gap-2 my-5 sm:grid-cols-1 sm:gap-1 md:grid-cols-2 md:grid-2 lg:grid-cols-2 lg:grid-2">
              <div>
                <Image
                  src={"/image/tintuc/tin-tuc-2.jpg"}
                  alt=""
                  width={550}
                  height={550}
                />
              </div>
              <div className="text-[#777] text-sm leading-7">
                <h3 className="text-start text-xl font-semibold text-black">
                  CÔNG NGHỆ EIR (EMBOSSED IN REGISTER)– SỰ “CHẠM KHẮC” HOÀN HẢO
                </h3>
                <p className="my-4">
                  Ứng dụng công nghệ EIR, Sàn đá công nghệ SPC Royal Crystal
                  kiến tạo nên chuẩn mực mới cho bề mặt sản phẩm, hướng đến cảm
                  nhận và trải nghiệm chân thực và hoàn mỹ nhất cho người dùng.
                </p>
                <p className="my-4">
                  EIR (Embossed-in-register) là công nghệ dập nổi bề mặt tiên
                  tiến nhất hiện nay. Chi tiết hơn, bằng công nghệ EIR, hiệu ứng
                  bề mặt được tạo nổi theo đường vân trên bề mặt sàn giống từng
                  lớp vân gỗ nhỏ của gỗ tự nhiên.
                </p>
                <p className="my-4">
                  Hơn nữa, những đường vân gỗ đi vào chi tiết, có chiều sâu tạo
                  sự chuyển biến màu sắc một cách rất tự nhiên cho bề mặt sàn.
                  Công nghệ tuyệt vời này tạo nên vẻ đẹp chân thực như gỗ thật,
                  đồng thời giúp tối ưu khả năng chống trơn trượt và chống trầy
                  xước của sàn.
                </p>
              </div>
            </div>
            <div className="!h-1 w-full bg-[#05372e]"></div>
            <div className="w-full grid grid-cols-2 gap-2 my-5 sm:grid-cols-1 sm:gap-1 md:grid-cols-2 md:grid-2 lg:grid-cols-2 lg:grid-2">
              <div>
                <Image
                  src={"/image/tintuc/tin-tuc-3.png"}
                  alt=""
                  width={550}
                  height={550}
                />
              </div>
              <div className="text-[#777] text-sm leading-7">
                <h3 className="text-start text-xl font-semibold text-black">
                  CÔNG NGHỆ CO-EXTRUSION – NÂNG TẦM CHUẨN MỰC SẢN PHẨM SÀN CAO
                  CẤP
                </h3>
                <p className="my-4">
                  Các dòng sản phẩm ván sàn thông thường chỉ có từ 3 – 4 lớp cơ
                  bản. Đối với sàn đá công nghệ SPC của RCG, sản phẩm sở hữu kết
                  cấu 5 lớp thông minh, bền chặt gồm: UV coating, Wear layer,
                  Decor film layer, SPC rigid core, Underlayment.
                </p>
                <p className="my-4">
                  Ứng dụng công nghệ Co-extrusion tân tiến, sàn đá công nghệ SPC
                  tiếp tục được RCG nâng cấp phát triển, kiến tạo một chuẩn mực
                  mới cho dòng sản phẩm cao cấp. Lớp cốt SPC sẽ được bổ sung
                  thêm 02 lớp hỗ trợ đắc lực, nâng tổng số lớp sản phẩm lên 07
                  lớp.
                </p>
                <p className="my-4">
                  Công nghệ Co-extrusion giúp tối ưu hóa khả năng chống co ngót
                  hay giãn nở, giúp Sàn đá công nghệ SPC Royal Crystal ổn định,
                  “bất biến” trước mọi biến động thời tiết hay nhiệt độ, cho sàn
                  đẹp bền bỉ vượt thời gian.
                </p>
              </div>
            </div>
            <div className="!h-1 w-full bg-[#05372e]"></div>
            <div className="w-full grid grid-cols-2 gap-2 my-5 sm:grid-cols-1 sm:gap-1 md:grid-cols-2 md:grid-2 lg:grid-cols-2 lg:grid-2">
              <div>
                <Image
                  src={"/image/tintuc/tin-tuc-4.jpg"}
                  alt=""
                  width={550}
                  height={550}
                />
              </div>
              <div className="text-[#777] text-sm leading-7">
                <h3 className="text-start text-xl font-semibold text-black">
                  CÔNG NGHỆ HÈM KHÓA BẢN QUYỀN TỪ UNILIN (BỈ) TẠO NÊN SỰ KHÁC
                  BIỆT
                </h3>
                <p className="my-4">
                  Với những thiết kế độc quyền, hoàn hảo, công nghệ hèm khóa
                  Uniclic từ tập đoàn Unilin (Bỉ) mang lại rất nhiều ưu điểm như
                  giúp giữ các tấm sàn chắc chắn, tạo lực liên kết lớn giữa
                  chúng mà không cần dùng keo dán. Bên cạnh đó, nó giữ cho sàn
                  không cong vênh, đảm bảo độ nở, khít khi có sự thay đổi về
                  nhiệt độ, độ ẩm môi trường.
                </p>
                <p className="my-4">
                  Công nghệ hèm khóa Unilin giúp cho việc lắp đặt sàn trở nên dễ
                  dàng, nhanh chóng, phát huy tối đa khả năng chống nước, chống
                  ẩm 100% và đem lại tính thẩm mỹ cao cho sàn. Việc đầu tư và sử
                  dụng dòng hèm khóa công nghệ cao này khẳng định sự khác biệt,
                  đẳng cấp của Sàn đá công nghệ SPC Royal Crystal của RCG.
                </p>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default ServiceComponent;
