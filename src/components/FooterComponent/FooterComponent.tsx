"use client";

const FooterComponent = () => {
    return (
        <div className="flex justify-center bg-gradient-to-r from-indigo-500 via-sky-500 via-30% to-emerald-500">
        <div className="w-10/12 sm:w-full flex justify-center items-center p-9 md:p-0 md:py-9 md:11/12">
          <div className="w-full grid grid-cols-2 gap-2 sm:grid-cols-none	md:grid-cols-2 md:gap-2">
            <div className="text-white">
              <h3 className="text-xl font-medium mb-4">Công ty cổ phần xuất nhập khẩu và thương mại LTC</h3>
              <ul>
                <li className="flex mb-2 text-white">
                  <img
                    src="/image/home/place.svg"
                    alt=""
                    className="w-6 h-6 mr-3"
                  />
                  <p>
                    243A Đê La Thành - Phường Láng Thượng - Quận Đống Đa - Thành Phố Hà Nội
                  </p>
                </li>
                <li className="flex text-white mb-2">
                  <img
                    src="/image/home/telephone.svg"
                    alt=""
                    className="w-6 h-6 mr-3"
                  />
                  <p>0366.683.747</p>
                </li>
              </ul>
            </div>
            <div className="text-white">
              <h3 className="text-xl font-medium mb-4">
                Sản phẩm doanh nghiệp
              </h3>
              <ul>
                <li className="mb-2">Sàn đá công nghệ cao SPC</li>
              </ul>
            </div>
          </div>
        </div>
      </div>
    );
}

export default FooterComponent