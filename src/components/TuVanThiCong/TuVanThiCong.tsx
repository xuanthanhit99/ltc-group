"use client";
import React, { useEffect, useState } from "react";
import { Tabs, TabsProps } from "antd";
import { useRouter } from "next/navigation";
import CardItem from "../CardItem/CardItem";
import Link from "next/link";
import Image from "next/image";
import ListProduct from "../ListProduct/ListProduct";
import productSanDa from "../../../utils/product.json"
const TuVanThiCong = (props: any) => {
    console.log("productSanDa?.tuVanThongTin", productSanDa?.tuVanThongTin);
  return (
    <div className="w-full h-auto">
      <div>
        <div className="bg-[url('/image/tuvanthicong/banner-tuvantt.jpg')] bg-cover w-full h-[700px] font-bold flex text-[#05372e] justify-center items-center text-4xl text-center">
            TƯ VẤN THI CÔNG
        </div>
        <div>
        <div className="flex justify-center">
          <div className="flex w-10/12 flex-col my-3">
          <div className="w-full h-14 mb-4  text-[#05372e] font-bold flex justify-start border-b border-[#00a2e9]  items-center lg:text-xl sm:text-sm text-center md:text-lg xl:text-4xl">
              <span className="">TƯ VẤN THI CÔNG</span>
            </div>
              <ListProduct valueproduct={productSanDa?.tuVanThongTin?.filter((item: any) => item?.type === "Tư Vấn Thông tin")} />
          </div>
        </div>
        <hr />
        <div className="flex justify-center">
          <div className="flex w-10/12 flex-col my-3">
          <div className="w-full h-14 mb-4  text-[#05372e] font-bold flex justify-start border-b border-[#00a2e9]  items-center lg:text-xl sm:text-sm text-center md:text-lg xl:text-4xl">
              <span className="">DỰ ÁN ĐÃ THỰC HIỆN</span>
            </div>
              <ListProduct valueproduct={productSanDa?.tuVanThongTin?.filter((item: any) => item?.type === "Dự án đã thực hiện")}    />
          </div>
        </div>
      </div>
      </div>
    </div>
  );
};

export default TuVanThiCong;
