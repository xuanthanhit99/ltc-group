"use client";
import React, { useEffect, useState } from "react";
import { Button, Tabs, TabsProps, notification } from "antd";
import { useRouter } from "next/navigation";
import CardItem from "../CardItem/CardItem";
import Link from "next/link";
import Image from "next/image";
import CustomInput from "../FormItemFloatLabel/CustomInput";
import CustomTextArea from "../FormItemFloatLabel/CustomTextArea";
import { AuthContextDefault } from "../../../app/context/AuthContext";

const LienHeComponent = (props: any) => {
    const [isShowListSocialMess, setIsShowListSocialMess] =
    useState<boolean>(false);
  const key = "formTT";
  const {
    onClickShowFormTT,
    isShowFormTT,
    onClickPostInformation,
    isResultFormTT,
  } = AuthContextDefault();
  const [name, setName] = useState<string>("");
  const [phone, setPhone] = useState<string>("");
  const [email, setEmail] = useState<string>("");
  const [textQuestion, setTextQuestion] = useState<string | undefined>("");
  const [api, contextHolder] = notification.useNotification();
  const onOpenLoading = () => {
    if (isResultFormTT) {
      api.open({
        key,
        message: "Gửi yêu cầu thành công",
        description: "Cảm ơn bạn đã gửi yêu cầu, chúng tôi sẽ liên hệ với bạn sớm nhất",
      });

      setTimeout(() => {
        api.open({
          key,
          message: "Gửi yêu cầu thành công",
          description: "Cảm ơn bạn đã gửi yêu cầu, chúng tôi sẽ liên hệ với bạn sớm nhất",
        });
      }, 1000);
    }
  };

  useEffect(() => {
    onOpenLoading();
  }, [isResultFormTT]);
  return (
    <div className="w-full h-auto">
      <div>
        <div className="bg-[url('/image/banner-gioithieu.jpg')] bg-cover w-full h-[700px] font-bold flex text-[#05372e] justify-center items-center text-4xl text-center">
          LIÊN HỆ CHÚNG TÔI
        </div>
        <div className="flex flex-col items-center justify-center">
          <div className="flex w-10/12 flex-col">
          <div className="w-full grid grid-cols-2 gap-2 my-5 sm:grid-cols-1 sm:gap-1 md:grid-cols-2 md:grid-2 lg:grid-cols-2 lg:grid-2">
              <div className="text-[#777] text-sm leading-7">
                <h3 className="text-start text-3xl font-semibold text-black">
                Công Ty Cổ Phần Xuất Nhập Khẩu Và Thương Mại VCM 
                </h3>
                <p className="my-4">
                    <span className="font-bold text-black">Trụ Sở Chính:</span> Số 1 Đỗ Hành, P. Nguyễn Du, Q. Hai Bà Trưng, TP. Hà Nội
                </p>
                <p className="my-4">
                    <span className="font-bold text-black">Địa chỉ:</span> Số 1 Đỗ Hành, P. Nguyễn Du, Q. Hai Bà Trưng, TP. Hà Nội
                </p>
                <p className="my-4">
                <span className="font-bold text-black">Email:</span> vcm.vphn@gmail.com
                </p>
                <p className="my-4">
                <span className="font-bold text-black">Hotline:</span> 0963.594.358
                </p>
                <p className="my-4">
                <span className="font-bold text-black">Website:</span> https://vcm-group.vn
                </p>
              </div>
              <div>
                <Image
                  src={"/image/tuvanthicong/tuvanthongtin-08.jpg"}
                  alt=""
                  width={550}
                  height={550}
                />
              </div>
            </div>
          <div className="z-10 sm:h-full ">
          <div className="w-full h-14 mb-4  text-[#05372e] font-bold flex justify-start border-b border-[#00a2e9]  items-center lg:text-xl sm:text-sm text-center md:text-lg xl:text-4xl">
              <span className="">THÔNG TIN LIÊN HỆ</span>
            </div>
            <div className="grid grid-cols-2 gap-2 my-5 sm:grid-cols-1 sm:gap-1 md:grid-cols-2 md:grid-2 lg:grid-cols-2 lg:grid-2 ">
              <div className="flex justify-center items-center">
                <Image
                  src={"/image/introduce/introduce-5.jpg"}
                  width={512}
                  height={435}
                  alt=""
                />
              </div>
              <div className="flex flex-col items-center mx-3">
                <h3 className="text-lg font-bold text-center mb-3">
                  Vui lòng để lại thông tin liên hệ
                  <br /> Máy lóc nước Hà nam sẽ tư vấn ngay cho bạn
                </h3>
                <div className="w-full">
                  <CustomInput
                    name=""
                    label="Họ và Tên"
                    onChange={(e) => setName(e?.target?.value)}
                  />
                </div>
                <div className="w-full">
                  <CustomInput
                    name=""
                    label="Số điện thoại"
                    onChange={(e) => setPhone(e?.target?.value)}
                  />
                </div>
                <div className="w-full">
                  <CustomInput
                    name=""
                    label="Email"
                    onChange={(e) => setEmail(e?.target?.value)}
                  />
                </div>
                <div className="w-full">
                  <CustomTextArea
                    label="Câu hỏi thường gặp"
                    onChange={(e) => setTextQuestion(e?.target?.value)}
                  />
                </div>
                <div>
                  <Button
                    type="primary"
                    size={"large"}
                    className="bg-red-600"
                    onClick={() =>
                      onClickPostInformation({ name, phone, textQuestion })
                    }
                  >
                    Gửi thông tin
                  </Button>
                </div>
              </div>
            </div>
          </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default LienHeComponent;
