"use client";
import CustomSelect from "@/components/FormItemFloatLabel/CustomSelect";
import ListProduct from "@/components/ListProduct/ListProduct";
import axios from "axios";
import { useEffect, useState } from "react";
import { getApiProduct } from "../context/QueryApi";
import { AuthContextDefault } from "../context/AuthContext";
import Image from "next/image";
import productSanDa from "../../utils/product.json";

const ProductPage = () => {
  const { isLoadingProduct, dataProduct } = AuthContextDefault();
  const [typeProduct, setTypeProduct] = useState<string>("");
  const [accessoryProduct, setAccessoryProduct] = useState<string>("");
  const [nameProduct, setNameProduct] = useState<string>("");
  return (
    <div>
      <div className="flex justify-center flex-col items-center">
        <div className="p-5 font-medium text-4xl text-white text-center flex justify-center items-center sm:text-lg banner-product">
          Tất cả các sản phẩm tại cửa hàng
        </div>
        <div className="flex w-full flex-col bg-[#05372e] justify-center items-center">
          <div className="flex w-10/12 flex-col justify-center items-center">
            <div className="w-full grid grid-cols-4 gap-4 my-5 sm:grid-cols-1 sm:gap-1 md:grid-cols-2 md:grid-2 lg:grid-cols-2 lg:grid-2">
              <div>
                <h3 className="text-white text-xl sm:text-base">
                  ROYAL HOUSE :
                </h3>
                <CustomSelect
                  label=""
                  onChange={(value) => setNameProduct(value)}
                  value={[{ value: "all", label: "Tất cả" }]}
                />
              </div>
              <div>
                <h3 className="text-white text-xl sm:text-base">
                  ROYAL CRYSTAL :
                </h3>
                <CustomSelect
                  label=""
                  onChange={(value) => setAccessoryProduct(value)}
                  value={[{ value: "all", label: "Tất cả" }]}
                />
              </div>
              <div>
                <h3 className="text-white text-xl sm:text-base">PHỤ KIỆN :</h3>
                <CustomSelect
                  label=""
                  onChange={(value) => setTypeProduct(value)}
                  value={[{ value: "all", label: "Tất cả" }]}
                />
              </div>
            </div>
          </div>
        </div>
      </div>
      <div>
        <div className="flex justify-center">
          <div className="flex w-10/12 flex-col my-3">
          <div className="w-full h-14 mb-4  text-[#05372e] font-bold flex justify-start border-b border-[#00a2e9]  items-center lg:text-xl sm:text-sm text-center md:text-lg xl:text-4xl">
              <span className="">ROYAL CRYSTAL</span>
            </div>
            {isLoadingProduct ? (
              <div className="flex justify-center items-center">
                <Image
                  src="/image/loading-2.gif"
                  width={350}
                  height={350}
                  alt="loader"
                  className="object-contain"
                />
              </div>
            ) : (
              <ListProduct valueproduct={productSanDa?.productSanDa?.filter((item: any) => item?.types === "crystal")} />
            )}
          </div>
        </div>
      </div>
      <div>
        <div className="flex justify-center">
          <div className="flex w-10/12 flex-col my-3">
          <div className="w-full h-14 mb-4  text-[#05372e] font-bold flex justify-start border-b border-[#00a2e9]  items-center lg:text-xl sm:text-sm text-center md:text-lg xl:text-4xl">
              <span className="">ROYAL HOUSE</span>
            </div>
            {isLoadingProduct ? (
              <div className="flex justify-center items-center">
                <Image
                  src="/image/loading-2.gif"
                  width={350}
                  height={350}
                  alt="loader"
                  className="object-contain"
                />
              </div>
            ) : (
              <ListProduct valueproduct={productSanDa?.productSanDa?.filter((item: any) => item?.types === "house")} />
            )}
          </div>
        </div>
      </div>
      <div>
        <div className="flex justify-center">
          <div className="flex w-10/12 flex-col my-3">
          <div className="w-full h-14  text-[#05372e] font-bold flex justify-start border-b border-[#00a2e9]  items-center lg:text-xl sm:text-sm text-center md:text-lg xl:text-4xl">
              <span className="">Phụ kiện</span>
            </div>
            {isLoadingProduct ? (
              <div className="flex justify-center items-center">
                <Image
                  src="/image/loading-2.gif"
                  width={350}
                  height={350}
                  alt="loader"
                  className="object-contain"
                />
              </div>
            ) : (
              <div className="h-[350px] w-full flex justify-center items-center text-center font-bold text-4xl bg-[#05372e] text-white sm:text-sm md:text-md lg:text-lg">
                <div>Sản phẩm đang trong quá trình phát triển</div>
              </div>
            )}
          </div>
        </div>
      </div>
    </div>
  );
};

export default ProductPage;
